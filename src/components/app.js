import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Dashboard from "../pages/dashboard";
import Settings from "../pages/settings";
import Navbar from "./navbar";
import history from "../history";
import LoadingBar from 'react-redux-loading-bar';

const App = () => (
	<BrowserRouter history={history}>
		<div>
			<Navbar />
			<LoadingBar
				style={{ backgroundColor: '#2e6da4' }}
				showFastActions
			/>
			<Route exact path="/" component={Dashboard} />
			<Route path="/settings" component={Settings} />
		</div>
	</BrowserRouter>
);

export default App;
