import React, { Component } from "react";
import { connect } from "react-redux";
import Device from "./device";

class Temperatures extends Component {
	renderDevices = () => {
		let devices = [];
		this.props.devices.forEach(device => {
			devices.push(<Device device={device} key={device.id} />);
		});

		return devices;
	};

	render() {
		console.log("rendering temperatures");
		return (
			<div id="devices-list" className="">
				{this.renderDevices()}
			</div>
		);
	}
}

const mapStateToProps = state => {
	const devices = new Map();

	state.devices.forEach(device => {
		if (
			device.device_type ===
			"urn:schemas-micasaverde-com:device:TemperatureSensor:1"
		) {
			devices.set(device.id, device);
		}
	});

	return { devices };
};

export default connect(mapStateToProps, null)(Temperatures);
