import React from "react";
import { FaStar, FaStarO } from "react-icons/lib/fa";
import {
	SwitchPower,
	Color,
	EnergyMetering,
	Dimming,
	TemperatureSensor
} from "./controls";
import * as Service from "./services";

const renderBody = device => {
	/*
	switch (device.device_type) {
		case 'urn:schemas-upnp-org:device:BinaryLight:1':
			return <SwitchPower device={device} />
	}
	*/

	let controls = [];

	for (let service in device.services) {
		const definition = device.services[service];

		switch (service) {
			case Service.EnergyMetering:
				controls.push(<EnergyMetering device={device} key={service} />);
				break;

			case Service.SwitchPower:
				controls.push(<SwitchPower device={device} key={service} />);
				break;

			case Service.Color:
				controls.push(<Color device={device} key={service} />);
				break;

			case Service.Dimming:
				controls.push(<Dimming device={device} key={service} />);
				break;

			case Service.Temperature:
				controls.push(
					<TemperatureSensor device={device} key={service} />
				);
				break;
		}
	}

	for (let childId in device.children) {
		const child = device.children[childId];
		controls.push(<Device device={child} key={childId} />);
	}

	return controls;
};

const Device = ({ device, favoriteHandler, isFavorite }) => {
	const icon = isFavorite ? <FaStar /> : <FaStarO />;
	return (
		<div className="card">
			<div className="card-body">
				<h4 className="card-title">
					{device.name}{" "}
					<small>
						({device.manufacturer} {device.model})
					</small>{" "}
					{favoriteHandler ? (
						<a onClick={favoriteHandler} className="float-right">
							{icon}
						</a>
					) : null}
				</h4>
				{renderBody(device)}
			</div>
		</div>
	);
};

export default Device;
