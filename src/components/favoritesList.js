import React, { Component } from "react";
import { connect } from "react-redux";
import {
	fetchFullState,
	addDeviceToFavorites,
	removeDeviceFromFavorites,
	updateStatus,
	getColor
} from "../actions";
import { bindActionCreators } from "redux";
import Device from "./device";
import { Color } from "./services";

class FavoritesList extends Component {
	renderDevices = () => {
		let devices = [];
		this.props.devices.forEach(device => {
			console.log(device);
			const favoriteHandler = this.props.removeDeviceFromFavorites.bind(
				null,
				device
			);
			devices.push(
				<Device
					device={device}
					isFavorite={true}
					favoriteHandler={favoriteHandler}
					key={device.id}
				/>
			);
		});

		return devices;
	};

	componentDidMount = () => {
		this.props.updateStatus();

		//window.setInterval(this.props.updateStatus, 10000);
		this.updateColors();

		window.setInterval(() => {
			this.props.updateStatus();
			this.updateColors(this.props.devices);
		}, 50000);
	};

	updateColors = () => {
		this.props.devices.forEach(device => {
			if (device.services.hasOwnProperty(Color)) {
				this.props.getColor(device);
			}
		});
	};

	render() {
		return (
			<div id="devices-list" className="card-columns">
				{this.renderDevices()}
			</div>
		);
	}
}

const mapStateToProps = state => {
	const devices = new Map();

	let position = 0;
	state.favorites.map(favoriteId => {
		devices.set(position, state.devices.get(favoriteId));
		position++;
	});
	return { devices };
};

const mapDispatchToProps = dispatch => {
	return bindActionCreators(
		{ removeDeviceFromFavorites, updateStatus, getColor },
		dispatch
	);
};

export default connect(mapStateToProps, mapDispatchToProps)(FavoritesList);
