import React, { Component } from "react";
import { connect } from "react-redux";
import {
	fetchFullState,
	addDeviceToFavorites,
	removeDeviceFromFavorites
} from "../actions";
import { bindActionCreators } from "redux";
import Device from "./device";
import ErrorBoundary from "./errorBoundary";

class DevicesList extends Component {
	renderDevices = () => {
		let devices = [];
		this.props.devices.forEach(device => {
			const isFavorite = this.props.favorites.indexOf(device.id) > -1;
			const favoriteHandler = isFavorite
				? this.props.removeDeviceFromFavorites.bind(null, device)
				: this.props.addDeviceToFavorites.bind(null, device);
			devices.push(
				<Device
					device={device}
					isFavorite={isFavorite}
					favoriteHandler={favoriteHandler}
					key={device.id}
				/>
			);
		});

		return devices;
	};

	render() {
		return (
			<div id="devices-list" className="card-columns">
				<a onClick={this.props.fetchFullState}>FETCH Devices</a>
				<ErrorBoundary>{this.renderDevices()}</ErrorBoundary>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		devices: state.devices,
		favorites: state.favorites
	};
};

const mapDispatchToProps = dispatch => {
	return bindActionCreators(
		{ fetchFullState, addDeviceToFavorites, removeDeviceFromFavorites },
		dispatch
	);
};

export default connect(mapStateToProps, mapDispatchToProps)(DevicesList);
