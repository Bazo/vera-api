import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

const mapStateToProps = (state, ownProps) => {
	let deviceStatus = state.status[ownProps.device.id];
	if (!deviceStatus) {
		deviceStatus = { status: 0 };
	}
	return {
		deviceStatus
	};
}

const mapDispatchToProps = (actionCreators) => {
	return (dispatch) => {
		return bindActionCreators(actionCreators, dispatch);
	}
}

const connectDevice = (component, actionCreators) => {
	return connect(mapStateToProps, mapDispatchToProps(actionCreators))(component);
};

export default connectDevice;