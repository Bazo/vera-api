import React from 'react';
import connectDevice from './connectDevice';

const EnergyMetering = ({ device, deviceStatus }) => {
	return (
		<div className="card-text">
		Watts: {deviceStatus.watts}
		</div>

	);
}


export default connectDevice(EnergyMetering, { });