import React from 'react';
import { switchPowerOn, switchPowerOff } from '../../actions';
import { FaLightbulbO } from 'react-icons/lib/fa';
import connectDevice from './connectDevice';

function componentToHex(c) {
	var hex = c.toString(16);
	return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex({ r, g, b }) {
	return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

const Color = ({ device, deviceStatus }) => {

	function createColorRGB() {
		const colors = { r: 0, g: 0, b: 0, w: 0 };
		if (deviceStatus.CurrentColor !== undefined) {
			const colorPairs = deviceStatus.CurrentColor.split(',');

			const pairing = { '0': 'w', '2': 'r', '3': 'g', '4': 'b' };

			colorPairs.map((pair) => {
				const valuePair = pair.split('=');
				const [key, value] = valuePair;

				const index = pairing[key];

				colors[index] = parseInt(value);
			});
		}
		return colors;
	}


	const on = deviceStatus.status == 1;

	return (
		<div className="rounded-circle" style={{ backgroundColor: rgbToHex(createColorRGB()), width: 45, height: 45 }}></div>
	);
}

export default connectDevice(Color, { switchPowerOn, switchPowerOff });