import React from 'react';
import { switchPowerOn, switchPowerOff } from '../../actions';
import { FaLightbulbO } from 'react-icons/lib/fa';
import connectDevice from './connectDevice';

const SwitchPower = ({ device, switchPowerOn, switchPowerOff, deviceStatus }) => {
	function turnOn() {
		switchPowerOn(device);
	}

	function turnOff() {
		switchPowerOff(device);
	}

	const on = deviceStatus.status == 1;

	return (
		<div className="card-text">
			<FaLightbulbO color={on ? 'gold' : 'grey'} size={64} />
			
			<button className="btn btn-success" onClick={turnOn}>
				ON
				</button>

			<button className="btn btn-danger" onClick={turnOff}>
				OFF
			</button>
		</div>

	);
}


export default connectDevice(SwitchPower, { switchPowerOn, switchPowerOff });