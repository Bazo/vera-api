import React, { Component } from 'react';
import { changeBrightness } from '../../actions';
import connectDevice from './connectDevice';
import Slider from 'react-rangeslider'

class Dimming extends Component {

	constructor(props) {
		super(props);

		const state = {
			level: parseInt(props.deviceStatus.level)
		};

		this.state = state;
	}

	componentWillReceiveProps(nextProps) {
		this.setState({ level: parseInt(nextProps.deviceStatus.level) });
	}

	onChange = (value) => {
		console.log(value);
		this.setState({ level: value });
	}

	onChangeComplete = () => {
		changeBrightness(this.props.device, this.state.level);
	}

	render() {
		return (
			<Slider
				min={0}
				max={100}
				step={1}
				value={this.state.level}
				onChange={this.onChange}
				onChangeComplete={this.onChangeComplete}
			/>
		);
	}
}

export default connectDevice(Dimming, { changeBrightness });