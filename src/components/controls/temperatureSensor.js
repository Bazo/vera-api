import React from 'react';
import connectDevice from './connectDevice';

const TemperatureSensor = ({ device, deviceStatus }) => {
	return (
		<span className="temperature">{deviceStatus.temperature}°</span>

	);
}


export default connectDevice(TemperatureSensor, { });