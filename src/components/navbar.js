import React from "react";
import { Link } from "react-router-dom";

const NavLink = ({ to, children }) => {
	return (
		<li className="nav-item">
			<Link className="nav-link" to={to}>
				{children}
			</Link>
		</li>
	);
};

const Navbar = () => (
	<nav className="navbar navbar-expand navbar-dark bg-dark">
		<a className="navbar-brand" href="#">
			VERA
		</a>

		<div>
			<ul className="navbar-nav mr-auto">
				<NavLink to="/">Dashboard</NavLink>
				<NavLink to="/settings">Settings</NavLink>
			</ul>
		</div>
	</nav>
);

export default Navbar;
