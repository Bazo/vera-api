import {
	SWITCH_POWER_ON,
	SWITCH_POWER_OFF,
	UPDATE_STATUS,
	GET_VARIABLE
} from "../actions";
import { REHYDRATE } from "redux-persist/constants";

const initialState = {};

export default function(state = initialState, action) {
	switch (action.type) {
		case REHYDRATE:
			return mapSavedState(action.payload.status, initialState);

		case SWITCH_POWER_ON:
			return updateVariable(state, action.device.id, "status", 1);

		case SWITCH_POWER_OFF:
			return updateVariable(state, action.device.id, "status", 0);

		case UPDATE_STATUS:
			return updateStatus(state, action.payload.data.devices);

		case GET_VARIABLE:
			return updateVariable(
				state,
				action.deviceId,
				action.variable,
				action.payload.data
			);
	}

	return state;
}

function mapSavedState(state, initialState) {
	if (state === undefined) {
		return initialState;
	}

	return state;
}

function updateStatus(state, devices) {
	let newState = state;

	devices.map(device => {
		newState[device.id] = { ...state[device.id], ...device };
	});

	return { ...newState };
}

function updateVariable(state, deviceId, variable, data) {
	let newState = state;
	newState[deviceId] = { ...state[deviceId], ...{ [variable]: data } };

	return { ...newState };
}
