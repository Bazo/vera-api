import { compose, createStore, applyMiddleware } from "redux";
import { persistStore, autoRehydrate, createTransform } from "redux-persist";
import promiseMiddleware from "redux-promise-middleware";
import { loadingBarMiddleware } from "react-redux-loading-bar";
import logger from "redux-logger";
import localForage from "localforage";

import reducers from "./reducers";

import { routerMiddleware } from "react-router-redux";

// Create a history of your choosing (we're using a browser history in this case)
import history from "./history";

// Build the middleware for intercepting and dispatching navigation actions
const routingMiddleware = routerMiddleware(history);

let middleware = [
	routingMiddleware,
	promiseMiddleware(),
	loadingBarMiddleware()
];

if (process.env.NODE_ENV === "development") {
	middleware.push(logger);
}

const store = createStore(
	reducers,
	undefined,
	compose(applyMiddleware(...middleware), autoRehydrate())
);

localForage.config({
	// driver      : localforage.WEBSQL, // Force WebSQL; same as using setDriver()
	name: "vera-ui",
	version: 1.0,
	//size        : 4980736, // Size of database, in bytes. WebSQL-only for now.
	storeName: "values", // Should be alphanumeric, with underscores.
	description: "Vera UI DB"
});

const devicesTransform = createTransform(
	// transform state coming from redux on its way to being serialized and stored
	(inboundState, key) => {
		return [...inboundState];
	},
	// transform state coming from storage, on its way to be rehydrated into redux
	(outboundState, key) => {
		return new Map(outboundState);
	},
	// configuration options
	{ whitelist: ["devices"] }
);

persistStore(store, { storage: localForage, transforms: [devicesTransform] });

export default store;
