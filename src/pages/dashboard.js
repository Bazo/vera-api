import React from "react";
import FavoritesList from "../components/favoritesList";
import Temperatures from "../components/temperatures";

const Dashboard = () => (
	<div id="dashboard" className="grid">
		<div className="box">
			<FavoritesList />
		</div>
		<div className="box">
			<Temperatures />
		</div>
	</div>
);

export default Dashboard;
