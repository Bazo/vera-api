//import "babel-polyfill";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import App from "./components/app";
import store from "./createStore";

import "../node_modules/react-rangeslider/lib/index.css";
import "../node_modules/bootstrap/dist/css/bootstrap.css";
const render = Component => {
	ReactDOM.render(
		<Provider store={store}>
			<Component />
		</Provider>,
		document.getElementById("root")
	);
};

render(App);
