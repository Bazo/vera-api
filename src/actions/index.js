import axios from "axios";

const VERA_API_URL = "/api";

export const FETCH_FULL_STATE = "FETCH_FULL_STATE";
export const ADD_DEVICE_TO_FAVORITES = "ADD_DEVICE_TO_FAVORITES";
export const REMOVE_DEVICE_FROM_FAVORITES = "REMOVE_DEVICE_FROM_FAVORITES";
export const SEND_ACTION = "SEND_ACTION";
export const SWITCH_POWER_ON = "SWITCH_POWER_ON";
export const SWITCH_POWER_OFF = "SWITCH_POWER_OFF";
export const UPDATE_STATUS = "UPDATE_STATUS";
export const CHANGE_BRIGHTNESS = "CHANGE_BRIGHTNESS";
export const GET_VARIABLE = "GET_VARIABLE";
export const SET_VARIABLE = "SET_VARIABLE";

export function addDeviceToFavorites(device) {
	return {
		type: ADD_DEVICE_TO_FAVORITES,
		device
	};
}

export function removeDeviceFromFavorites(device) {
	return {
		type: REMOVE_DEVICE_FROM_FAVORITES,
		device
	};
}

export function fetchFullState() {
	const params = {
		id: "user_data"
	};

	const request = axios.get(VERA_API_URL, {
		params
	});

	return {
		type: FETCH_FULL_STATE,
		payload: request
	};
}

export function updateStatus() {
	const params = {
		id: "sdata"
	};

	const request = axios.get(VERA_API_URL, {
		params
	});

	return {
		type: UPDATE_STATUS,
		payload: request
	};
}

export function getColor(device) {
	return getVariable(
		device,
		"urn:micasaverde-com:serviceId:Color1",
		"CurrentColor"
	);
}

export function updateColor(device, color) {}

export function getVariable(device, service, variable) {
	const params = {
		id: "variableget",
		DeviceNum: device.id,
		Variable: variable,
		serviceId: service
	};

	const request = axios.get(VERA_API_URL, {
		params
	});

	return {
		type: GET_VARIABLE,
		payload: request,
		meta: {
			variable: variable,
			deviceId: device.id
		}
	};
}

export function setVariable(device, service, variable, value) {
	const params = {
		id: "variableset",
		DeviceNum: device.id,
		Variable: variable,
		serviceId: service,
		value: value
	};

	const request = axios.get(VERA_API_URL, {
		params
	});

	return {
		type: SET_VARIABLE,
		payload: request
	};
}

export function sendAction(params, type) {
	params.id = "action";

	const request = axios.get(VERA_API_URL, {
		params
	});

	return {
		type: type,
		payload: request
	};
}

export function switchPowerOn(device) {
	const params = getSwitchPowerParams(device.id, 1);
	params.id = "action";

	const request = axios.get(VERA_API_URL, {
		params
	});

	return {
		type: SWITCH_POWER_ON,
		payload: request,
		meta: { device }
	};
}

export function switchPowerOff(device) {
	const params = getSwitchPowerParams(device.id, 0);
	params.id = "action";

	const request = axios.get(VERA_API_URL, {
		params
	});

	return {
		type: SWITCH_POWER_OFF,
		payload: request,
		meta: { device }
	};
}

function getSwitchPowerParams(deviceNum, value) {
	return {
		DeviceNum: deviceNum,
		serviceId: "urn:upnp-org:serviceId:SwitchPower1",
		action: "SetTarget",
		newTargetValue: value
	};
}

export function changeBrightness(device, value) {
	const params = {
		DeviceNum: device.id,
		serviceId: "urn:upnp-org:serviceId:Dimming1",
		action: "SetLoadLevelTarget",
		newLoadlevelTarget: value
	};

	sendAction(params, CHANGE_BRIGHTNESS);
}
