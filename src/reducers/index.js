import { combineReducers } from "redux";
import devicesReducer from "./devices";
import favoriteDevicesReducer from "./favorite_devices";
import statusReducer from "./status";
//import { routerReducer } from 'react-router-redux'

export default combineReducers({
	devices: devicesReducer,
	favorites: favoriteDevicesReducer,
	//router: routerReducer,
	status: statusReducer
});
