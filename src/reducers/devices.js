import { FETCH_FULL_STATE } from "../actions";
import { REHYDRATE } from "redux-persist/es/constants";
import pick from "lodash/pick";
import sortBy from "lodash/sortBy";

const initialState = new Map();

export default function(state = initialState, action) {
	switch (action.type) {
		case REHYDRATE: {
			const state = mapRehydratedDevices(
				action.payload.devices,
				initialState
			);
			console.log(state);
			return state;
		}
		case `${FETCH_FULL_STATE}_FULFILLED`: {
			const state = mapRemoteDevices(action.payload.data.devices);
			console.log(state);
			return state;
		}
	}

	return state;
}

function mapRehydratedDevices(devices, initialState) {
	console.log(devices, initialState);
	if (devices === undefined) {
		return initialState;
	}
	console.log(Map, devices, initialState);
	return new Map(devices);
}

function mapDevice(device) {
	const services = mapServices(device);
	let props = pick(device, [
		"id",
		"name",
		"id_parent",
		"altid",
		"device_type",
		"manufacturer",
		"model",
		"room",
		"disabled",
		"local_udn",
		"ip",
		"category_num"
	]);
	props.services = services;
	props.children = {};
	return props;
}

function mapRemoteDevices(devices) {
	const map = new Map();
	devices.map(device => {
		let props = mapDevice(device);

		if (
			props.device_type === "urn:schemas-upnp-org:device:DimmableLight:1"
		) {
			const parent = map.get(device.id_parent);
			if (
				parent.device_type ===
				"urn:schemas-upnp-org:device:DimmableRGBLight:2"
			) {
				parent.children[device.id] = props;
				map.set(device.id_parent, parent);
			}
		}

		map.set(device.id, props);
	});
	console.log(map);
	return map;
}

function mapServices(device) {
	const controlUrls = device.ControlURLs;

	let services = {}; //new Map();
	for (let serviceNo in controlUrls) {
		const serviceDefinition = controlUrls[serviceNo];

		const serviceName = serviceDefinition.service.split(":").pop();

		services[serviceName] = serviceDefinition;
		//services.set(serviceName, serviceDefinition);
	}

	return services;
}
