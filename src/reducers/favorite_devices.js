import {
	ADD_DEVICE_TO_FAVORITES,
	REMOVE_DEVICE_FROM_FAVORITES
} from "../actions";
import { REHYDRATE } from "redux-persist/es/constants";
import mapKeys from "lodash/mapKeys";
import without from "lodash/without";
import concat from "lodash/concat";
const initialState = [];

export default function(state = initialState, action) {
	switch (action.type) {
		case REHYDRATE:
			if (action.payload.favorites) {
				return concat(state, action.payload.favorites);
			}
			break;

		case ADD_DEVICE_TO_FAVORITES:
			return concat(state, action.device.id);

		case REMOVE_DEVICE_FROM_FAVORITES:
			return without(state, action.device.id);
	}

	return state;
}
