package main

/*
func GetAppsHandler(w http.ResponseWriter, r *http.Request, db *mgo.Database) {
	var apps []App
	db.C("App").Find(nil).All(&apps)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(apps)
}

func GetActionsHandler(w http.ResponseWriter, r *http.Request, db *mgo.Database) {
	actions := []string{}
	db.C("Log").Find(nil).Distinct("action", &actions)
	sort.Strings(actions)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(actions)
}

func GetLogsHandler(w http.ResponseWriter, r *http.Request, db *mgo.Database) {
	var params LogsRequest
	decoder := json.NewDecoder(r.Body)
	decoder.Decode(&params)

	maxItemsPerPage := 50
	skip := (params.Page - 1) * maxItemsPerPage

	logs := []Log{}

	query := bson.M{
		"action":    bson.M{"$in": params.Actions},
		"appName":   bson.M{"$in": params.Apps},
		"result":    bson.M{"$in": params.Statuses},
		"timestamp": bson.M{"$gte": params.DateFrom.Unix(), "$lte": params.DateTo.Unix()},
	}

	if len(params.Oses) > 0 {
		query["os"] = bson.M{"$in": params.Oses}
	}

	if len(params.ComID) > 0 {
		query["comId"] = params.ComID
	}

	q := db.C("Log").Find(query).Sort("-timestamp")
	totalHits, _ := q.Count()

	q.Limit(maxItemsPerPage).Skip(skip).All(&logs)

	pagesCount := int(math.Ceil(float64(totalHits) / float64(maxItemsPerPage)))

	var nextPage interface{}
	var previousPage interface{}
	if params.Page < pagesCount {
		nextPage = params.Page + 1
	}

	if params.Page > 1 {
		previousPage = params.Page - 1
	}

	payload := LogsResponse{
		TotalHits:    totalHits,
		PagesCount:   pagesCount,
		Page:         params.Page,
		Results:      logs,
		NextPage:     nextPage,
		PreviousPage: previousPage,
		MaxPerPage:   params.MaxPerPage,
	}

	sendResponse(w, payload)
}

func toString(i int) string {
	return fmt.Sprintf("%d", i)
}

func sendResponseWithHeaders(w http.ResponseWriter, v interface{}, headers map[string]string) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	for header, value := range headers {
		w.Header().Set(header, value)
	}

	log.Println(w.Header())

	json.NewEncoder(w).Encode(v)
}

func sendResponse(w http.ResponseWriter, v interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	json.NewEncoder(w).Encode(v)
}
*/
