package main

import (
	"flag"
	"log"
	"net/http"
	"os"
)

func main() {

	configFile := flag.String("file", "vera-api.yml", "configuration file")
	config := LoadConfig(configFile)

	if len(os.Args) > 1 {
		log.Println(os.Args)
	}

	log.Println(config)

	createRouter(config)

	log.Fatal(http.ListenAndServe(":"+config.Port, nil))

}
