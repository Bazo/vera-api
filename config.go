package main

import (
	"github.com/jinzhu/configor"
)

/*
{
		"user_data",
		"status",
		"sdata",
		"actions",
		"device",
		"scene",
		"room",
		"file",
		"lua",
		"action",
		"variableset",
		"variableget",
		"reload",
		"alive",
		"finddevice",
		"resync",
		"wget",
		"iprequests",
		"blacklistip",
		"live_energy_usage",
		"request_image",
		"archive_video",
		"jobstatus",
		"invoke",
		"relay",
		"update_plugin",
	}
*/

//Config for the app
type Config struct {
	Vera struct {
		IP           string `default:"192.168.0.227"`
		Port         string `default:"3480"`
		LuupRequests []string
	}

	Port string `default:"8000"`
}

func LoadConfig(configFile *string) *Config {
	var config Config
	configor.Load(&config, *configFile)
	config.Vera.LuupRequests = []string{
		"user_data",
		"status",
		"sdata",
		"actions",
		"device",
		"scene",
		"room",
		"file",
		"lua",
		"action",
		"variableset",
		"variableget",
		"reload",
		"alive",
		"finddevice",
		"resync",
		"wget",
		"iprequests",
		"blacklistip",
		"live_energy_usage",
		"request_image",
		"archive_video",
		"jobstatus",
		"invoke",
		"relay",
		"update_plugin",
	}
	return &config
}
