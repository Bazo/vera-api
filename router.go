package main

import (
	"io/ioutil"
	"log"
	"net/http"

	"net/url"

	"github.com/GeertJohan/go.rice"
)

func createRouter(config *Config) {
	box := rice.MustFindBox("build")

	http.HandleFunc("/api", func(w http.ResponseWriter, r *http.Request) {
		params := r.URL.Query()

		params.Add("output_format", "json")

		veraURL := url.URL{
			Scheme:   "http",
			Host:     config.Vera.IP + ":" + config.Vera.Port,
			Path:     "data_request",
			RawQuery: params.Encode(),
		}

		url := veraURL.String()

		resp, err := http.Get(url)

		log.Println("resp", resp.Status, resp.StatusCode)
		if err == nil {
			body, err := ioutil.ReadAll(resp.Body)
			if err == nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusOK)
				w.Write(body)
			} else {
				log.Println("read error", err)
			}
		} else {
			log.Println("get error", err)
		}
		defer resp.Body.Close()

	})

	http.Handle("/", http.FileServer(box.HTTPBox()))
}
