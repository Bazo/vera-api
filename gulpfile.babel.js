/* eslint-env node */

"use strict";

import gulp from "gulp";
import runSequence from "run-sequence";
import less from "gulp-less";
import notify from "gulp-notify";
import util from "gulp-util";

import child from "child_process";
import os from "os";
const sync = require("gulp-sync")(gulp).sync;

var server = null;

gulp.task("server:build", function() {
	// Build application in the "gobin" folder
	var build = child.spawnSync("go", ["install"]);

	if (build.stderr.length) {
		util.log(util.colors.red("Something wrong with this version :"));
		var lines = build.stderr
			.toString()
			.split("\n")
			.filter(function(line) {
				return line.length;
			});
		for (var l in lines)
			util.log(util.colors.red("Error (go install): " + lines[l]));
		notify.onError({
			title: "Error (go install)",
			message: lines
		});
	}

	return build;
});

// Server launch
gulp.task("server:spawn", function() {
	// Stop the server
	if (server && server !== "null") {
		server.kill();
	}

	// Application name
	if (os.platform() == "win32") {
		// Windows
		var path_folder = __dirname.split("\\");
	} else {
		// Linux / MacOS
		var path_folder = __dirname.split("/");
	}
	var length = path_folder.length;
	var app = path_folder[length - parseInt(1)];

	// Run the server
	if (os.platform() == "win32") {
		server = child.spawn(app + ".exe");
	} else {
		server = child.spawn(app);
	}

	// Display terminal informations
	server.stderr.on("data", function(data) {
		process.stdout.write(data.toString());
	});
});

// Watch files
gulp.task("server:watch", function() {
	gulp.watch(
		["*.go", "**/*.go"],
		sync(["server:build", "server:spawn"], "server")
	);
});

gulp.task("server", ["server:build", "server:spawn", "server:watch"]);
